import "./App.scss";
import Menu from "./components/mainPart/Menu";
import PageFooter from "./components/mainPart/PageFooter";
import PageMain from "./components/mainPart/PageMain";

function App() {
  return (
    <>
      <Menu />
      <PageMain />
      <PageFooter />
    </>
  );
}

export default App;
