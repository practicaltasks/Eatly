import ButtonMain from "./ButtonMain";
import SvgTrust from "./svg/SvgTrust";

import SvgStar from "./svg/SvgStar";
export default function HeroText() {
  return (
    <div className="hero-text">
      <p className="hero-text__text-over">OVER 1000 USERS</p>
      <div className="hero-text__content">
        <h1 className="hero-text__title">
          Enjoy Foods All Over The{" "}
          <span className="hero-text__span-title">World</span>
        </h1>
        <p className="hero-text__text">
          EatLy help you set saving goals, earn cash back offers, Go to
          disclaimer for more details and get paychecks up to two days early.
          Get a <span className="hero-text__span-text">$20 bonus</span>.
        </p>
      </div>
      <div className="hero-text__wrap-btn">
        <ButtonMain name="Get Started" onClick={() => {}} />
        <button type="button"  className="hero-text__btn btn">Go Pro</button>
      </div>
      <div className="hero-text__trust">
        <SvgTrust />
        <SvgStar />
        <p className="hero-text__cnt">4900+</p>
      </div>
    </div>
  );
}
