export default function ButtonMain(props: {
  name: string | any;
  onClick: () => void;
}) {
  return (
    <button type="button" className="btn btn--signup" onClick={props.onClick}>
      {props.name}
    </button>
  );
}
