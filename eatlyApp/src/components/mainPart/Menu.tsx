import { useState } from "react";
import ButtonMain from "../ButtonMain";
import Link from "../Link";
import SignUpPP from "../pp/SignUpPP";
import SvgLogo from "../svg/SvgLogo";

export default function Menu() {
  let [ppActive, setPpActive] = useState<boolean>(false);
  let [navActive, setNav] = useState(false)
  return (
    <header className="menu">
      <div className="menu__container">
        <div className="menu__main">
          <a className="menu__wrap-logo">
            <SvgLogo />
          </a>
          <div className={navActive? "menu__wrap-nav--active": "menu__wrap-nav"}>
          <nav className="menu__nav">
            <ul className={navActive? "menu__ul--active": "menu__ul"}>
              <li className="menu__li">
                <Link name="Menu" />
              </li>
              <li className="menu__li">
                <Link name="Blog" />
              </li>
              <li className="menu__li">
                <Link name="Pricing" />
              </li>
              <li className="menu__li">
                <Link name="Contact" />
              </li>
            </ul>
          </nav>
          <div className="menu__wrap-auth">
            <button
              className="btn btn--login"
              onClick={() => setPpActive(true)}
            >
              Login
            </button>
            <ButtonMain name="Sign up" onClick={() => {}} />
          </div>
          </div>
          
          <button type="button" className="menu__burger" onClick={() => setNav(true)}>
            <span className="menu__line"></span>
            <span className="menu__line"></span>
            <span className="menu__line"></span>
          </button>
        </div>
      </div>
      <SignUpPP active={ppActive} setActive={setPpActive} />
    </header>
  );
}
