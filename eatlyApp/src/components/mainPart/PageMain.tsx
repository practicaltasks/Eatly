import SectionCustomerSay from "../sections/pageMain/SectionCustomerSay";
import SectionDownload from "../sections/pageMain/SectionDownload";
import SectionFeatures from "../sections/pageMain/SectionFeatures";
import SectionHeros from "../sections/pageMain/SectionHeros";
import SectionPurchase from "../sections/pageMain/SectionPurchase";
import SectionRestaurants from "../sections/pageMain/SectionReastaurants";
import SectionSubscribe from "../sections/pageMain/SectionSubscribe";
import SectionTopDishes from "../sections/pageMain/SectionTopDishes";

export default function PageMain() {
  return (
    <main className="main">
      <SectionHeros />
      <SectionFeatures />
      <SectionDownload />
      <SectionRestaurants />
      <SectionTopDishes />
      <SectionPurchase />
      <SectionCustomerSay />
      <SectionSubscribe />
    </main>
  );
}
