import HeroFood from "../../HeroFood";
import HeroText from "../../HeroText";
export default function SectionHeros() {
  return (
    <section className="section-heros">
      <div className="section-heros__container">
        <HeroText />
        <HeroFood />
      </div>
    </section>
  );
}
