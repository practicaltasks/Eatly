import CardFood from "../../card/CardFood";
export default function SectionTopDishes() {
  return (
    <section className="rest">
      <div className="rest__container">
        <h2 className="rest__title title">
          Our Top <span className="rest__span">Dishes</span>
        </h2>

        <ul className="rest__card">
          <CardFood
            title="Chicken Hell"
            img="../../src/assets/img/f4.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
            priceb={12}
            prices={99}
          />
          <CardFood
            title="Swe Dish"
            img="../../src/assets/img/f5.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
            priceb={12}
            prices={99}
          />
          <CardFood
            title="Swe Dish"
            img="../../src/assets/img/f4.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
            priceb={12}
            prices={99}
          />
          <CardFood
            title="Chicken Hell"
            img="../../src/assets/img/f5.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
            priceb={12}
            prices={99}
          />
          <CardFood
            title="Swe Dish"
            img="../../src/assets/img/f4.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
            priceb={12}
            prices={99}
          />
        </ul>
        <a href="" className="rest__link">
          View All
          <svg
            className="svg__arrow rest__svg"
            viewBox="0 0 28 28"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M6.55469 13.5469H22.3588"
              strokeWidth="2.25773"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M14.4561 5.64453L22.3581 13.5466L14.4561 21.4487"
              strokeWidth="2.25773"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
          ,
        </a>
      </div>
    </section>
  );
}
