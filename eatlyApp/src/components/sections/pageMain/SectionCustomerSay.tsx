import CardReview from "../../card/CardReview";

import "swiper/css";
export default function SectionCustomerSay() {
  return (
    <section className="customer">
      <div className="customer__container">
        <h2 className="customer__title title">
          <span className="customer__span title__title-span">Customer</span> Say
        </h2>
        <ul className="customer__card">
          <CardReview
            img="./../src/assets/Pic.png"
            title="Alexander R."
            text="01 Year With Us "
            content="“ Online invoice payment helps companies save time, are faster and save maximum effort for the clients and save maximum effort. Online invoice payment helps companies save time ”"
          />
          <CardReview
            img="./../src/assets/Pic.png"
            title="Alexander R."
            text="01 Year With Us"
            content="“ Online invoice payment helps companies save time, are faster and save maximum effort for the clients and save maximum effort. Online invoice payment helps companies save time ”"
          />
          <CardReview
            img="./../src/assets/Pic.png"
            title="Alexander R."
            text="01 Year With Us "
            content="“ Online invoice payment helps companies save time, are faster and save maximum effort for the clients and save maximum effort. Online invoice payment helps companies save time ”"
          />
        </ul>
        <ul className="customer__ul">
          <li className="customer__li">
            <button type="button"  className="customer__btn"></button>
          </li>
          <li className="customer__li">
            <button type="button"  className="customer__btn"></button>
          </li>
          <li className="customer__li">
            <button type="button"  className="customer__btn"></button>
          </li>
        </ul>
      </div>
    </section>
  );
}
