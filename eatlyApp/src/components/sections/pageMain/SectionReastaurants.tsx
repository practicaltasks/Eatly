import CardRest from "../../card/CardRest";
export default function SectionRestaurants() {
  return (
    <section className="rest">
      <div className="rest__container">
        <h2 className="rest__title">
          Our Top <span className="rest__span">Restaurants</span>
        </h2>

        <ul className="rest__card">
          <CardRest
            title="The Chicken King"
            img="../../src/assets/img/f2.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
          />
          <CardRest
            title="The Burger King"
            img="../../src/assets/img/f1.png"
            time={24}
            highlight="Trending"
            mark={4.9}
          />
          <CardRest
            title="The Chicken King"
            img="../../src/assets/img/f2.png"
            time={24}
            highlight="Healthy"
            mark={4.8}
          />
        </ul>
        <a href="" className="rest__link">
          View All
          <svg
            className="svg__arrow rest__svg"
            viewBox="0 0 28 28"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M6.55469 13.5469H22.3588"
              strokeWidth="2.25773"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M14.4561 5.64453L22.3581 13.5466L14.4561 21.4487"
              strokeWidth="2.25773"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
          ,
        </a>
      </div>
    </section>
  );
}
