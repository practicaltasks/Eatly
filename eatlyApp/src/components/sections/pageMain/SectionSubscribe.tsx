import ButtonMain from "../../ButtonMain";
export default function SectionSubscribe() {
  return (
    <section className="subscribe">
      <div className="subscribe__container">
        <div className="subscribe__wrap">
          <h2 className="subscribe__title">GET 50%</h2>
          <div className="subscribe__form">
            <input
              type="text"
              className="subscribe__input"
              placeholder="Email Address"
            />

            <ButtonMain name="SUBSCRIBE" onClick={() => {}} />
          </div>
          <div className="subscribe__wrap-img">
            <img
              src="./../src/assets/img/f5.png"
              alt=""
              className="subscribe__img"
            />
          </div>
        </div>
      </div>
    </section>
  );
}
