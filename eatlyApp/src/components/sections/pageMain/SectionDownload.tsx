import mob from "../../../assets/mob.png";
import ButtonMain from "../../ButtonMain";
export default function SectionDownload() {
  return (
    <section className="download">
      <div className="download__container">
        <div className="download__wrap">
          <img src={mob} alt="" className="download__img" />
        </div>
        <div className="download__content">
          <h2 className="download__title">
            Premium <span className="download__span">Quality</span> For Your
            Health
          </h2>
          <ul className="download__ul">
            <li className="download__text">
              Premium quality food is made with ingredients that are packed with
              essential vitamins, minerals.
            </li>
            <li className="download__text">
              These foods promote overall wellness by support healthy digestion
              and boosting immunity
            </li>
          </ul>
          <ButtonMain
            onClick={() => {}}
            name={[
              "Download",
              <svg
                key={1}
                className="svg__arrow btn__svg"
                viewBox="0 0 28 28"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6.55469 13.5469H22.3588"
                  strokeWidth="2.25773"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M14.4561 5.64453L22.3581 13.5466L14.4561 21.4487"
                  strokeWidth="2.25773"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>,
            ]}
          />
        </div>
      </div>
    </section>
  );
}
