import SvgLinesLeft from "../../svg/SvgLinesLeft";
import SvgLinesRight from "../../svg/SvgLinesRight";
export default function SectionFeatures() {
  return (
    <section className="features">
      <div className="features__line">
        <SvgLinesLeft />
        <SvgLinesRight />
      </div>
      <ul className="features__ul">
        <li className="features__li">
          <h2 className="features__title">10K+</h2>
          <p className="features__text">
            Satisfied Costumers All Great Over The World{" "}
          </p>
        </li>
        <li className="features__li features__li--m ">
          <h2 className="features__title">4M</h2>
          <p className="features__text">
            Healthy Dishes Sold Including Milk Shakes Smooth
          </p>
        </li>
        <li className="features__li">
          <h2 className="features__title">99.99%</h2>
          <p className="features__text">
            Reliable Customer Support We Provide Great Experiences
          </p>
        </li>
      </ul>
    </section>
  );
}
