export default function Link(props: { name: string }) {
  return (
    <>
      <a href="" className="link">
        {props.name}
      </a>
    </>
  );
}
