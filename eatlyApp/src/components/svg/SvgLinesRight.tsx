export default function SvgLinesRight() {
  return (
    <div className="svg__wrap-line svg__wrap-line--r">
      <svg
        className="svg__line"
        viewBox="0 0 128 221"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          opacity="0.2"
          d="M250.547 -80.8447C227.068 -78.6528 201.569 -78.3091 178.691 -73.183C156.305 -68.1671 132.729 -59.0673 113.033 -49.4564C76.737 -31.7454 58.1474 -6.95103 41.2821 22.6297C13.4496 71.4467 3.3729 122.078 2.93791 175.123C2.64995 210.239 3.99988 244.997 7.66528 279.998C8.78437 290.684 8.22504 302.9 16.0695 312.128"
          stroke="#A596FF"
          strokeWidth="4.44874"
          strokeLinecap="round"
        />
      </svg>
      <svg
        className="svg__line"
        viewBox="0 0 88 221"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          opacity="0.2"
          d="M258.001 -55.6357C233.819 -53.3776 207.556 -53.0235 183.993 -47.7427C160.937 -42.5754 136.655 -33.2008 116.37 -23.2998C78.9867 -5.05417 59.8405 20.4887 42.4704 50.9625C13.8045 101.253 3.42611 153.413 2.9781 208.059C2.68151 244.235 4.07186 280.042 7.84701 316.1C8.9996 327.109 8.42353 339.694 16.5028 349.2"
          stroke="#A596FF"
          strokeWidth="4.44874"
          strokeLinecap="round"
        />
      </svg>
    </div>
  );
}
