import graph from "../../assets/Graph.png";
import food from "../../assets/food.png";
export default function SignUpPP(props: {
  active: boolean;
  setActive: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  return (
    <div
      className={props.active ? "signup signup--active body__signup" : "signup"}
      onClick={() => props.setActive(false)}
    >
      <div className="signup__container" onClick={(e) => e.stopPropagation()}>
        <div className="signup__container-left">
          <div className="signup__logo">
            <svg
              width="53"
              height="49"
              viewBox="0 0 53 49"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect
                x="4.23877"
                y="5.74316"
                width="48.761"
                height="43.0728"
                rx="8.04025"
                fill="#6C5FBC"
              />
              <rect
                x="1.16228"
                y="1.16228"
                width="46.4364"
                height="40.7482"
                rx="6.87797"
                fill="#DBD9EE"
                stroke="#6C5FBC"
                strokeWidth="2.32456"
              />
              <path
                d="M37.6362 13.9023L11.2162 13.7855"
                stroke="#6C5FBC"
                strokeWidth="2.40454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M37.6199 19.1922L31.744 19.1662C29.3136 19.1555 27.348 16.7766 27.3569 13.8568C27.3659 10.937 29.346 8.57558 31.7765 8.58632L37.6523 8.6123"
                stroke="#6C5FBC"
                strokeWidth="2.40454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M20.0285 30.8277L12.3518 30.7422C11.7228 30.7394 11.1957 30.1188 11.1838 29.3631L11.1327 27.3706C11.1208 26.5805 11.6518 25.9131 12.3094 25.916L19.9003 26.0011L20.0285 30.8277Z"
                stroke="#6C5FBC"
                strokeWidth="2.40454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M32.0865 33.6975L21.2357 33.5636C20.6066 33.5608 20.0796 32.9402 20.0676 32.1844L19.9293 25.9835L37.5566 26.1988L37.5684 26.989C37.657 30.7336 35.1888 33.7455 32.0865 33.6975Z"
                stroke="#6C5FBC"
                strokeWidth="2.40454"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
          <form className="signup__form form" action="">
            <h1 className="form__title">Sign In To eatly</h1>
            <div className="form__accounts">
              <button type="button"  className="form__btn">G</button>
              <button type="button"  className="form__btn">
                <svg
                  width="27"
                  height="31"
                  viewBox="0 0 27 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_1_11797)">
                    <path
                      d="M25.5299 24.2333C25.0708 25.282 24.5274 26.2474 23.8978 27.1349C23.0396 28.3448 22.3369 29.1823 21.7954 29.6474C20.9559 30.4107 20.0565 30.8017 19.0933 30.8239C18.4019 30.8239 17.568 30.6294 16.5974 30.2347C15.6236 29.8419 14.7286 29.6474 13.9103 29.6474C13.0521 29.6474 12.1317 29.8419 11.1472 30.2347C10.1612 30.6294 9.36685 30.835 8.75955 30.8554C7.83594 30.8943 6.91533 30.4923 5.9964 29.6474C5.4099 29.1415 4.6763 28.2744 3.79747 27.046C2.85456 25.7341 2.07936 24.2129 1.47206 22.4787C0.821651 20.6054 0.495605 18.7915 0.495605 17.0353C0.495605 15.0237 0.935205 13.2887 1.81571 11.8347C2.50772 10.6669 3.42833 9.74565 4.58054 9.06936C5.73276 8.39307 6.97773 8.04843 8.31845 8.02639C9.05205 8.02639 10.0141 8.25077 11.2096 8.69175C12.4017 9.13421 13.1672 9.35859 13.5028 9.35859C13.7537 9.35859 14.604 9.09622 16.0455 8.57316C17.4087 8.08809 18.5593 7.88724 19.5018 7.96635C22.0558 8.17017 23.9746 9.16571 25.2507 10.9593C22.9665 12.3278 21.8366 14.2446 21.8591 16.7035C21.8797 18.6188 22.5824 20.2126 23.9634 21.4781C24.5892 22.0655 25.2882 22.5194 26.0658 22.8418C25.8972 23.3254 25.7192 23.7886 25.5299 24.2333ZM19.6723 1.44133C19.6723 2.94254 19.1177 4.3442 18.0121 5.64157C16.678 7.18389 15.0642 8.07512 13.3143 7.93448C13.292 7.75439 13.279 7.56484 13.279 7.36566C13.279 5.9245 13.9135 4.38219 15.0402 3.12113C15.6028 2.48264 16.3182 1.95174 17.1858 1.52823C18.0515 1.11104 18.8703 0.880329 19.6405 0.84082C19.663 1.04151 19.6723 1.24221 19.6723 1.44132V1.44133Z"
                      fill="#323142"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1_11797">
                      <rect
                        width="25.5702"
                        height="30.0172"
                        fill="white"
                        transform="translate(0.495605 0.839844)"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </button>
            </div>
            <p className="form__or">or</p>
            <input type="text" className="form__input" placeholder="Email" />
            <input type="text" className="form__input" placeholder="Password" />
            <a className="form__forget">Forget Password ?</a>
            <button type="submit"  className="form__submit">SIGN IN</button>
            <a href="" className="form__other">
              Create A New Account?
              <span className="form__span"> Sign Un</span>
            </a>
          </form>
          <div className="signup__wrap-text">
            <p className="signup__text-b">Privacy Policy</p>
            <p className="signup__text-b">Copyright 2022</p>
          </div>
        </div>
        <div className="signup__container-right">
          <div className="hero-food">
            <div className="hero-food__wrap-img-fb hero-food__wrap-img-fb--s">
              <div className="hero-food__top-card hero-food__top-card--s">
                <div className="hero-food__left">
                  <div className="hero-food__wrap-img-fm hero-food__wrap-img-fm--s">
                    <img src={food} alt="food" className="hero-food__img" />
                  </div>
                  <div className="hero-food__alltext">
                    <h3 className="hero-food__text1 hero-food__text1--s">
                      Chicken Hell
                    </h3>
                    <p className="hero-food__text2 hero-food__text2--s">
                      On The Way
                    </p>
                  </div>
                </div>
                <p className="hero-food__time">3:09 PM</p>
              </div>
              <img src={food} alt="food" className="hero-food__img" />
            </div>

            <div className="hero-food__wrap-img hero-food__wrap-img--s">
              <img src={graph} alt="food" className="hero-food__img" />
            </div>
          </div>
          <h2 className="signup__title">Find Foods With Love </h2>
          <p className="signup__text">
            Eatly Is The Food Delivery Dashboard And Having More Than 2K+ Dishes
            Including Asian, Chinese, Italians And Many More. Our Dashboard
            Helps You To Manage Orders And Money.
          </p>
        </div>
      </div>
    </div>
  );
}
