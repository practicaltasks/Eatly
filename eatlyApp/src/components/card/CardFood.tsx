import SvgHeart from "../svg/SvgHeart";
import SvgPlus from "../svg/SvgPlus";
import SvgStarOne from "../svg/SvgStarOne";
export default function CardFood(props: {
  img: string;
  title: string;
  highlight: string;
  time: number;
  mark: number;
  priceb: number;
  prices: number;
}) {
  const colors: any = {
    Trending: "card-rest__trending",
    Healthy: "card-rest__healthy",
    Supreme: "card-rest__supreme",
  };
  function Colors(highlight: string) {
    for (let i in colors) {
      if (i === highlight) return colors[i];
    }
  }
  return (
    <li className="card-food">
      <div className="card-food__top">
        <div className="card-food__wrap-img">
          <img src={props.img} alt="" className="card-food__img" />
          <SvgHeart />
        </div>
      </div>

      <div className="card-rest__wrap-info">
        <div className={Colors(props.highlight) + " card-rest__highlight"}>
          <p>{props.highlight}</p>
        </div>
        <h4 className="card-rest__title">{props.title}</h4>
        <div className="card-rest__wrap-text">
          <p className="card-rest__info card-rest__info--point">
            {props.time}min -
          </p>
          <SvgStarOne />
          <p className="card-rest__info">{props.mark}</p>
        </div>
        <div className="card-food__cost">
          <p className="card-food__price">
            ${props.priceb}
            <span className="card-food__span">.{props.prices}</span>
          </p>
          <button className="card-food__btn">
            {" "}
            <SvgPlus />
          </button>
        </div>
      </div>
    </li>
  );
}
