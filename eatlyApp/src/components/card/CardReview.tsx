import SvgCov from "../svg/SvgKov";
import SvgStar from "../svg/SvgStar";
export default function CardReview(props: {
  img: string;
  title: string;
  text: string;
  content: string;
}) {
  return (
    <li className="card-review">
      <div className="card-review__wrap">
        <div className="card-review__wrap-img">
          <img src={props.img} alt="" className="card-review__img" />
        </div>

        <div className="card-review__info">
          <h3 className="card-review__title">{props.title}</h3>
          <p className="card-review__text">{props.text}</p>
        </div>
        <SvgCov />
      </div>

      <p className="card-review__content">{props.content}</p>
      <SvgStar />
    </li>
  );
}
