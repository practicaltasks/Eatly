export default function CardStatistic(props: {
  svg1: string;
  title: string;
  text: string;
  price: number;
}) {
  return (
    <li className="card-statistic">
      <div className="card-statistic__wrap-svg">{props.svg1}</div>
      <div className="card-statistic__wrap-info">
        <h4 className="card-statistic__title">{props.title}</h4>
        <p className="card-statistic__text">{props.text}</p>
      </div>

      <p className="card-statistic__price">${props.price}</p>
    </li>
  );
}
