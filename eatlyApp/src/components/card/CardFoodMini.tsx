export default function CardFoodMini(props: {
  img: string;
  title: string;
  text: string;
  time: string;
}) {
  return (
    <li className="card-foodmini__top-card">
      <div className="card-foodmini__left">
        <div className="card-foodmini__wrap-img-fm">
          <img src={props.img} alt="food" className="card-foodmini__img" />
        </div>
        <div className="card-foodmini__alltext">
          <h4 className="card-foodmini__text1">{props.title}</h4>
          <p className="card-foodmini__text2">{props.text}</p>
        </div>
      </div>
      <p className="card-foodmini__time">{props.time}</p>
    </li>
  );
}
