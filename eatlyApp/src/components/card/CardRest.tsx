import SvgBookMark from "../svg/SvgBookMark";
import SvgStarOne from "../svg/SvgStarOne";
export default function CardRest(props: {
  img: string;
  title: string;
  highlight: string;
  time: number;
  mark: number;
}) {
  const colors: any = {
    Trending: "card-rest__trending",
    Healthy: "card-rest__healthy",
    Supreme: "card-rest__supreme",
  };
  function Colors(highlight: string) {
    for (let i in colors) {
      if (i === highlight) return colors[i];
    }
  }
  return (
    <li className="card-rest">
      <div className="card-rest__wrap-img">
        <img src={props.img} alt="" className="card-rest__img" />
      </div>
      <div className="card-rest__wrap-info">
        <div className={Colors(props.highlight) + " card-rest__highlight"}>
          <p>{props.highlight}</p>
        </div>
        <h4 className="card-rest__title">{props.title}</h4>
        <div className="card-rest__wrap-text">
          <p className="card-rest__info card-rest__info--point">
            {props.time}min -
          </p>
          <SvgStarOne />
          <p className="card-rest__info">{props.mark}</p>
        </div>
        <button className="card-rest__wrap-bookmark">
          <SvgBookMark />
        </button>
      </div>
    </li>
  );
}
