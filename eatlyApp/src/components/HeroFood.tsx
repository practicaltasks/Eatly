import graph from "../assets/Graph.png";
import food from "../assets/food.png";
export default function HeroFood() {
  return (
    <div className="hero-food">
      <div className="hero-food__wrap-img-fb">
        <div className="hero-food__top-card">
          <div className="hero-food__left">
            <div className="hero-food__wrap-img-fm">
              <img src={food} alt="food" className="hero-food__img" />
            </div>
            <div className="hero-food__alltext">
              <h3 className="hero-food__text1">Chicken Hell</h3>
              <p className="hero-food__text2">On The Way</p>
            </div>
          </div>
          <p className="hero-food__time">3:09 PM</p>
        </div>
        <img src={food} alt="food" className="hero-food__img" />
      </div>

      <div className="hero-food__wrap-img">
        <img src={graph} alt="food" className="hero-food__img" />
      </div>
    </div>
  );
}
