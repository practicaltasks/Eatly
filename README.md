# Eatly

## Описание проекта

Eatly - это веб-приложение для заказа еды. Главная страница предоставляет пользователю обзор доступных блюд и услуг, а также позволяет ознакомиться с ценами. 

## Версия node js

v20.9.0

## Установка и запуск проекта

- Клонировать репозиторий: git clone https://gitlab.com/practicaltasks/Eatly.git
- Перейти в папку проекта: cd eatlyApp
- Установить зависимости: npm install
- Запустить frontend: npm run dev
- Открыть приложение в браузере: http://localhost:5173

